# README #
#
Author: Pablo González Martínez
#
pablogonzmartinez@gmail.com

Unity3D Engine
C#

This project  showcases the implementation of a camera system for a playable environment.
It includes and allows to test three different camera modes all implemented into the same script to allow for smooth changes between them via damp functions.
To allow testing, a third person character controller via rigidbody has also been created, working with a third party animated model.

# Google Docs doc #
https://docs.google.com/document/d/1hSXrp3yblv6KEjP7S4GIdnbTr__uKh1VPj46FSIseYE/edit?usp=sharing

# Requirements #
The project has been created with Unity 2017.1.1f1 (64-bit).

# After-download guide: #
It contains no .exe, as the execution of the project itself ignoring the code is not very relevant. It should be opened in Unity using the parent folder containing ‘Assets’ and ‘Project Settings’ folders.

The project is organized in folders. The main scene is labelled as ‘test’, and is contained in the Scenes folder.

All the code is contained in the Scripts folder and has been created exclusively by the author.
