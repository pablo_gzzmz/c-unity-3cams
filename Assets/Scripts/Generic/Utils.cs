
using UnityEngine;

public class Utils {

    public class Math {

        public static Vector3 QBezier(Vector3 start, Vector3 end, Vector3 controlPoint, float t) {
            Vector3 result = Vector3.zero;

            result.x = (1 - t) * (1 - t) * start.x + 2 * (1 - t) * t * controlPoint.x + t * t * end.x;
            result.y = (1 - t) * (1 - t) * start.y + 2 * (1 - t) * t * controlPoint.y + t * t * end.y;
            result.z = (1 - t) * (1 - t) * start.z + 2 * (1 - t) * t * controlPoint.z + t * t * end.z;

            return result;
        }

        public static float QBezier(float start, float end, float controlPoint, float t) {
            return (1 - t) * (1 - t) * start + 2 * (1 - t) * t * controlPoint + t * t * end;
        }
    }
}
