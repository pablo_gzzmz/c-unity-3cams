
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Simple component to show/hide info image with key input
/// </summary>

public class Info : MonoBehaviour {

    [SerializeField]
    private GameObject show;

    [SerializeField]
    private GameObject close;

    [SerializeField]
    private GameObject image;

    private void Update() {
        if (Input.GetKeyDown(KeyCode.H)) {
            if (show.activeSelf) {
                Show();
            }

            else {
                Close();
            }
        }
    }

    public void Show() {
        show.SetActive(false);
        close.SetActive(true);
        image.SetActive(true);
    }

    public void Close() {
        show.SetActive(true);
        close.SetActive(false);
        image.SetActive(false);
    }
}
