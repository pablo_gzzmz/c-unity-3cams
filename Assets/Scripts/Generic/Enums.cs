
public enum CameraMode {
    ThirdPerson,
    TopDown,
    Free,
    Count
}

public enum PlayerInputMode {
    ThirdPerson,
    TopDown,
    Free
}