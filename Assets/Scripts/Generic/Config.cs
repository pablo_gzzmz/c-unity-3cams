
using UnityEngine;

public static class Config {
    
    private static ConfigProfile profile;
    
    public static float FreeCameraSpeed   { get { return profile.FreeCameraSpeed; } }
    public static float MouseSensitivity  { get { return profile.MouseSensitivity; } }
    public static float GamepadSensitiviy { get { return profile.GamepadSensitivity; } }

    public static bool  LockMouseMovement { get { return profile.LockMouseMovement; } }
    public static bool  Gamepad           { get { return profile.Gamepad; } }

    static Config() {
        profile = Resources.Load<ConfigProfile>("Profiles/Config Profile");
        profile.Set();
    }
}