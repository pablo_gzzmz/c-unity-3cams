
using UnityEngine;

public static class Globals {

    public const float maxHeight = 50;

    public const float thirdPersonSideOffsetAmount = 0.75f;

    public static readonly Vector2 placeholderMapSize = new Vector2(32, 32);

}
