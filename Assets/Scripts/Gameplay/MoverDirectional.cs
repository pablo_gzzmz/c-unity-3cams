
using UnityEngine;

/// <summary>
/// Component for an object that can move with directional input using a rigidbody.
/// </summary>

[RequireComponent(typeof(Rigidbody))]
public class MoverDirectional : MonoBehaviour {
    
    private const float rotationSpeed = 0.085f;
    private const float sprintMultiplier = 2.5f;

    private bool locked;
    public  bool Locked { get { return locked; } set { locked = value; } }

    // A reference can be used as a base XZ direction
    [SerializeField]
    private bool useReference;
    [SerializeField]
    private Transform referenceTransform;

    private Rigidbody rb;
    public  Rigidbody RB { get { return rb; } }

    [SerializeField]
    private float speed = 1;
    public  float Speed { set { speed = value; sprintSpeed = speed * sprintMultiplier; } }

    private bool  sprinting;
    public  bool  Sprinting { get { return sprinting; } set { sprinting = value; } }
    private float sprintSpeed;

    private Vector3 direction;

    private void Awake() {
        rb = GetComponent<Rigidbody>();
        rb.freezeRotation = true;

        Speed = speed;
    }

    public void Move(float z, float x) {
        if (locked) return;

        float multiplier = (sprinting ? sprintSpeed : speed);

        direction = Vector3.zero;

        if (z != 0 || x != 0) {
            // Diagonal speed reduction
            if (!Config.Gamepad && z != 0 && x != 0) multiplier *= 0.71f;
               
            // Use the reference transform to establish directional vectors
            if (useReference) {
                Vector3 referenceForward = GetReferenceForward();
                Vector3 referenceRight = new Vector3(referenceForward.z, 0, -referenceForward.x);

                direction = referenceRight * x + referenceForward * z;
            }
            else {
                direction = Vector3.right * x + Vector3.forward * z;
            }
            
            // Rotation via Slerp
            Quaternion rot = Quaternion.LookRotation(direction);
            transform.rotation = Quaternion.Slerp(transform.rotation, rot, rotationSpeed);

            // Set the direction to take into account relevant values to be used for rb's velocity
            direction.x *= multiplier;
            direction.z *= multiplier;
        }

        direction.y = rb.velocity.y;
        rb.velocity = direction;
    }

    private Vector3 GetReferenceForward() {
        // Ensure the reference calculation ignores any rotation on x axis
        Vector3 euler = referenceTransform.eulerAngles;
        Vector3 originalEuler = euler;
        euler.x = 0;
        referenceTransform.eulerAngles = euler;

        Vector3 forward = referenceTransform.forward;

        referenceTransform.eulerAngles = originalEuler;

        return forward;
    }
}
