
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Simple component used to test the implemented character animator.
/// </summary>

[RequireComponent(typeof(MoverDirectional))]
public class MoverAnimator : MonoBehaviour {

    [SerializeField]
    private Animator animator;

    [SerializeField]
    private float dampTime = 0.15f;

    private int speedID;

    private float currentSpeed;
    private float speedChangeVelocity;

    private MoverDirectional mover;

    private void Awake() {
        mover = GetComponent<MoverDirectional>();

        speedID  = Animator.StringToHash("Speed");
    }

    private void FixedUpdate () {
        float targetSpeed = mover.RB.velocity.magnitude;

        // Since the directional mover has been coded to be responsive and thus changes non-smoothly,
        // smoothness should be achieved by damping the value used for the animator itself.
        currentSpeed = Mathf.SmoothDamp(currentSpeed, targetSpeed, ref speedChangeVelocity, dampTime, 5000, Time.fixedDeltaTime);

        animator.SetFloat(speedID, currentSpeed);
	}
}
