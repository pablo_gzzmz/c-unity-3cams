
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

/// <summary>
/// Component class for minimap management.
/// </summary>
    
[RequireComponent(typeof(RectTransform))]
[RequireComponent(typeof(RawImage))]
public class Minimap : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerEnterHandler, IPointerExitHandler {
        
    private const int originalTextureWidth  = 256;
    private const int originalTextureHeight = 256;

    [SerializeField]
    private Camera cam = null;

    private static RawImage image;
    private static RenderTexture renderTexture;

    private static RectTransform   rectTransform;

    private static Vector2 originalSize;

    private static bool travelToCursor;
    private static bool hovered;

    private void Start() {
        cam.enabled = true;

        Map.OnInitialization += Set;
        ScreenController.OnScreenSizeChange += Set;

        rectTransform = GetComponent<RectTransform>();
        originalSize  = rectTransform.sizeDelta;

        Debug.Assert(originalSize.x == originalSize.y);

        image = GetComponent<RawImage>();
    }
        
    private void OnDestroy() {
        Map.OnInitialization -= Set;
    }
        
    private void Set() {
        float ratio = Map.Active.Size.y / Map.Active.Size.x;

        if(renderTexture != null) renderTexture.Release();

        if (ratio > 1) {
            rectTransform.sizeDelta  = new Vector2(Mathf.Round(originalSize.x / ratio), originalSize.y);
            renderTexture = new RenderTexture((int)(originalTextureWidth / ratio), originalTextureHeight, 16);
        }
        else 
        if (ratio < 1) {
            rectTransform.sizeDelta  = new Vector2(originalSize.x, Mathf.Round(originalSize.y * ratio));
            renderTexture = new RenderTexture(originalTextureWidth, (int)(originalTextureHeight * ratio), 16);
            image.texture = renderTexture;
        }
        else {
            rectTransform.sizeDelta  = originalSize;
            renderTexture = new RenderTexture(originalTextureWidth, originalTextureHeight, 16);
        }
            
        cam.targetTexture = renderTexture;
        image.texture = renderTexture;
        cam.orthographicSize = Map.Active.Size.y * 0.5f;
        cam.transform.position = new Vector3(Map.Active.Size.x * 0.5f, Globals.maxHeight, Map.Active.Size.y * 0.5f);
        cam.farClipPlane = Globals.maxHeight + 1;
    }

    public void OnPointerDown (PointerEventData data) { travelToCursor = true;  } 
    public void OnPointerUp   (PointerEventData data) { travelToCursor = false; }
    public void OnPointerEnter(PointerEventData data) { hovered = true; }
    public void OnPointerExit (PointerEventData data) { hovered = false; }

    private void Update() {
        if (travelToCursor && hovered) TravelToCursor();
    }

    private void TravelToCursor() {
        float min = rectTransform.anchoredPosition.x;
        float max = rectTransform.anchoredPosition.x + rectTransform.sizeDelta.x;
        float sub = max - min;
        float xt = (Input.mousePosition.x - min) / sub;
        min = rectTransform.anchoredPosition.y;
        max = rectTransform.anchoredPosition.y + rectTransform.sizeDelta.y;
        sub = max - min;
        float yt = (Input.mousePosition.y - min) / sub;

        Vector3 destiny = new Vector3(xt * Map.Active.Size.x, 0, yt * Map.Active.Size.y);
        LocalPlayer.View.FreeJumpTo(destiny);
    }
}
