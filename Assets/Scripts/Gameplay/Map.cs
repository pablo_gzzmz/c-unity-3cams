
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Map {

	private static Bounds bounds;
    public  static Bounds Bounds { get { return bounds; } }

    private static Map active;
    public  static Map Active { get { return active; } }

    public static event Action OnInitialization;

    private Vector2 size;
    public  Vector2 Size { get { return size; } }

    static Map() {
        active = new Map(Globals.placeholderMapSize);

        bounds = new Bounds(Vector2.zero, Globals.placeholderMapSize);

        if (OnInitialization != null) OnInitialization.Invoke();
    }

    public Map(Vector2 _size) {
        size = _size;
    }
}
