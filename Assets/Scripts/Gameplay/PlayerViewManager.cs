
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Drawing;
using Cursor = System.Windows.Forms.Cursor;

public class PlayerViewManager : MonoBehaviour {
    
    private CameraMode mode = CameraMode.Count;
    public  CameraMode Mode { get { return mode; } }

    private new Camera camera;
    public      Camera Camera { get { return camera; } }

    #region Common
    [Header("Common parameters")]
    [SerializeField]
    private bool enableInput = true;
    public  bool EnableInput {
        get { return enableInput; }
        set { enableInput = value; }
    }

    [SerializeField]
    private bool invertY = true;
    public  bool InvertY { get { return invertY; } }

    [SerializeField]
    private Transform target;
    public  Transform Target { get { return target; } set { target = value; } }

    // To smoothly follow the target via damping
    private const float smoothFollowDampTime = 0.2f;
    private Vector3 smoothFollowVel;

    private Vector3 targetPoint;

    private Vector3 targetOffset;
    private Vector3 currentOffset;
    private Vector3 currentOffsetVel;

    private float targetDistance;
    private float currentDistance;
    private float currentDistanceVel;

    private float targetRotation = 45;
    private float currentRotation;
    private float currentRotationVel;
    
    private float targetPitch = 35;
    private float currentPitch;
    private float currentPitchVel;

    private bool fixCollision;
    private bool tryOverhead;

    [SerializeField]
    private float dampTime = 0.1f;

    private const float basePitch = 25;
    private const float baseRotation = 0;
    #endregion

    #region Third person variables
    [Space(8)]
    [Header("Third person parameters")]
    [SerializeField]
    private Vector3 thirdPersonTargetOffset;

    [Range(1, 20)]
    [SerializeField]
    private float thirdPersonDistance = 2.5f;
    
    [Range(-89, 89)]
    [SerializeField]
    private float thirdPersonMaxPitch = 89;

    [Range(-89, 89)]
    [SerializeField]
    private float thirdPersonMinPitch = -89;
    #endregion

    #region Top down variables
    [Space(8)]
    [Header("Top down parameters")]
    [Range(1, 20)]
    [SerializeField]
    private float topDownDistance = 8;
    
    [SerializeField]
    private Vector2 topDownAngle = new Vector3(60, 0);
    #endregion

    #region Free camera variables
    /// <summary> Distance to screen bounds considered to start scrolling. </summary>
    private const float scrollArea = 6f;
    
    private const float maxFreeDistance = 20f;
    private const float minFreeDistance = 10f;
    private const float minFreePitch  = 35;
    private const float maxFreePitch  = 89.9f;

    private bool rotating;

    private Vector3 freeTarget;   // Vector acting as target when in free camera
    private Vector3 freeMovDirection;
    private Vector3 hitTarget;    // Target with height equal to hitpoint height after spherecasting
    private Vector3 spherecaster; // Position with highest height for spherecasting
    
    /// <summary> Current distance to viewing target by the 'Free' camera. </summary>
    private  float freeDistance  = 14f;
    
    // For smooth height calculation based on groud sphere collision detection
    private float currentHeightVel = 0;

    /// <summary> Array of screen-scroll movement bounds. Used for mouse movement. </summary>
    private  float[] scrollBounds = new float[3] { scrollArea, 0, 0 };

    private LayerMask freeCameraHitMask;

    private  Point mouseHiddenOrigin;

    public  event Action OnStartedRotating;
    public  event Action OnStoppedRotating;
    #endregion

    #region Collision & overhead variables
    private LayerMask cameraCollisiongMask;
    private const float collisionOffset = 0.1f;

    private bool  colliding = false;
    /// <summary> Used to control the damping of distance </summary>
    private float collisionFixDistance;
    private float collisionFixDistanceVel;

    [Space(6)]
    [Header("Overhead")]
    // Maximum offset for collision fix overhead when close to target
    [SerializeField]
    private float overheadHeight = 0.75f;
    [SerializeField]
    private float overheadDistance = 2f;
    private float sqrOverheadDistance;
    #endregion

    private void Reset() {
        // For camera components I prefer this method rather than RequireComponent.
        if (GetComponent<Camera>() == null) {
            Debug.LogError("This component should be placed only in a gameobject with a camera. Deleting.");
            UnityEditor.EditorApplication.delayCall += () => {
                DestroyImmediate(this);
            };
        }
    }

    private void OnValidate() {
        // To avoid collision fix problems, ensure the target offset height is always above the target's ground
        if (thirdPersonTargetOffset.y <= 0) thirdPersonTargetOffset.y = 0.05f;

        sqrOverheadDistance = overheadDistance * overheadDistance;
    }

    private void Start() {
        Debug.Assert(target != null, "A target needs to be specified before Start.");
        
        currentDistance = thirdPersonDistance;

        targetRotation  = baseRotation;
        targetPitch     = basePitch;
        currentRotation = targetRotation;
        currentPitch    = targetPitch;

        cameraCollisiongMask = LayerController.GroundMask;
        freeCameraHitMask    = LayerController.GroundMask;

        camera = GetComponent<Camera>();
        Debug.Assert(camera != null);

        ScreenController.OnScreenSizeChange += SetScrollBounds;
        SetScrollBounds();
        
        SetCameraMode(CameraMode.ThirdPerson);
    }

    private void OnDestroy() {
        ScreenController.OnScreenSizeChange -= SetScrollBounds;
    }

    private void FixedUpdate() {
        switch (mode) {
            case CameraMode.ThirdPerson:
                targetOffset   = thirdPersonTargetOffset;
                targetDistance = thirdPersonDistance;

                SetTargetPointBySmoothFollow();
                PerformAllDampings();
                break;

            case CameraMode.TopDown:
                targetDistance = topDownDistance;

                SetTargetPointBySmoothFollow();
                PerformAllDampings();
                break;

            case CameraMode.Free:
                PerformDistanceDamping();
                break;
        }
    }

    private void OnPreRender() {
        if (mode == CameraMode.Free) {
            if (rotating) PerformMouseRotation();

            SetTargetPointByFreeTarget();
        }

        MaintainRotationBounds();

        PerformCameraPositioning();

        if (fixCollision)
            CameraCollisionFixer(target.transform.position + new Vector3(0, currentOffset.y, 0));

        if (tryOverhead)
            TryPerformOverhead(currentOffset.y);
    }
    
    /// <summary> 
    /// Set the camera mode changing appropiate parameters. 
    /// </summary>
    public void SetCameraMode(CameraMode _mode) {
        Debug.Assert(_mode != CameraMode.Count, "WTF?");

        if (_mode == mode) return;

        mode = _mode;
        currentDistanceVel = 0;

        switch (_mode) {
            #region Third person
            case CameraMode.ThirdPerson:
                fixCollision = true;
                tryOverhead  = true;
                break;
            #endregion

            #region Top down
            case CameraMode.TopDown:
                targetRotation = Mathf.Abs(targetRotation - topDownAngle.y) < 180 ? topDownAngle.y : topDownAngle.y + 360;
                targetPitch    = topDownAngle.x;
                
                targetOffset   = Vector3.zero;

                fixCollision = false;
                tryOverhead  = false;
                colliding = false;
                collisionFixDistance = 0;
                break;
            #endregion

            #region Free
            case CameraMode.Free:
                targetDistance = freeDistance;
                
                // Get the correct height
                freeTarget  = targetPoint;
                hitTarget.y = targetPoint.y;

                fixCollision = false;
                tryOverhead  = false;
                colliding = false;
                collisionFixDistance = 0;
                targetOffset = Vector3.zero;

                // Fix pitch if outside bounds
                targetPitch  = Mathf.Clamp(targetPitch, minFreePitch, maxFreePitch);
                currentPitch = targetPitch;
                
                targetDistance = freeDistance;
                break;
            #endregion
        }
    }
    
    /// <summary> 
    /// Set scrolling bounds for 'Free' Camera movement via mouse on screen edges.
    /// </summary>
    private  void SetScrollBounds() {
        scrollBounds[1] = Screen.width  - scrollArea;
        scrollBounds[2] = Screen.height - scrollArea;
    }

    /// <summary>
    /// Set the target-dependant camera modes via smooth damp
    /// </summary>
    private void SetTargetPointBySmoothFollow() {
        targetPoint = Vector3.SmoothDamp(targetPoint, target.transform.position, ref smoothFollowVel, smoothFollowDampTime, 5000, Time.fixedDeltaTime);
    }

    /// <summary>
    /// Set the 'Free' mode camera target via spherecasting and finding the correct height.
    /// </summary>
    private void SetTargetPointByFreeTarget() {
        // Collision check towards target
        spherecaster = freeTarget;
        spherecaster.y = Globals.maxHeight;
        
        hitTarget.x = freeTarget.x;
        hitTarget.z = freeTarget.z;

        // Get height via spherecast
        RaycastHit hit;
        if (Physics.SphereCast(spherecaster, 2f, Vector3.down, out hit, Mathf.Infinity, freeCameraHitMask)) {
            hitTarget.y = Mathf.SmoothDamp(hitTarget.y, hit.point.y, ref currentHeightVel, dampTime, 5000, Time.unscaledDeltaTime);
        }

        targetPoint = hitTarget;
    }
    
    /// <summary>
    /// Add rotation and pitch amount. Used for input rotation in third person.
    /// </summary>
    public void AddThirdPersonRotation(float addedRotation, float addedPitch) {
        targetRotation += addedRotation;
        targetPitch     = Mathf.Clamp(targetPitch + addedPitch, thirdPersonMinPitch, thirdPersonMaxPitch);;
    }

    public void SetThirdPersonXOffset(float amount) {
        thirdPersonTargetOffset.x = amount;
    }

    /// <summary>
    /// Performs camera movement for the 'Free' Camera mode based on input, including mouse on screen edges movement if available. 
    /// </summary>
    public void MoveFreeTarget(float inputZ, float inputX) {
        Vector2 mouse = Input.mousePosition;

        float finalSpeed = Config.FreeCameraSpeed * Time.unscaledDeltaTime;
        
        freeMovDirection = Vector2.zero;

        //Mouse input
        if (!Config.LockMouseMovement) {
            if (mouse.x <  scrollBounds[0]) inputX = -1;
            else
            if (mouse.x >= scrollBounds[1]) inputX =  1;

            if (mouse.y <  scrollBounds[0]) inputZ = -1;
            else
            if (mouse.y >= scrollBounds[2]) inputZ =  1;
        }

        if (inputX != 0 && inputZ != 0) finalSpeed *= 0.71f; // Diagonal modifier

        freeMovDirection.x = inputX * finalSpeed;
        freeMovDirection.z = inputZ * finalSpeed;
        freeMovDirection = Quaternion.Euler(0, currentRotation, 0) * freeMovDirection;

        freeTarget += freeMovDirection;
        MaintainFreeTargetBounds();
    }

    /// <summary>
    /// Maintain the 'Free' Camera target position inside bounding.
    /// </summary>
    private  void MaintainFreeTargetBounds() {
        Bounds bounds = Map.Bounds;

        if (freeTarget.x < bounds.Start.x) freeTarget.x = bounds.Start.x;
        else
        if (freeTarget.x > bounds.End.x)   freeTarget.x = bounds.End.x;

        if (freeTarget.z < bounds.Start.y) freeTarget.z = bounds.Start.y;
        else
        if (freeTarget.z > bounds.End.y)   freeTarget.z = bounds.End.y;
    }

    /// <summary>
    /// Add or substract an amount to the current 'Free' Camera mode distance.
    /// </summary>
    public void SetFreeZoom(float zoom) {
        if (zoom != 0) {
            freeDistance = Mathf.Clamp(freeDistance - zoom * 5, minFreeDistance, maxFreeDistance);

            targetDistance = freeDistance;
        }
    }

    /// <summary>
    /// Performs an instant camera jump to the given position if the camera mode is 'Free'.
    /// </summary>
    public void FreeJumpTo(Vector3 position) {
        if (mode != CameraMode.Free) return;
        
        freeTarget = position;
    }

    /// <summary>
    /// Calculates the rotation and pitch of the camera based on cursor movement.
    /// </summary>
    private void PerformMouseRotation() {
        float multiplier = Config.MouseSensitivity;

        targetRotation += (Cursor.Position.X - mouseHiddenOrigin.X) * multiplier;
        targetPitch    += (Cursor.Position.Y - mouseHiddenOrigin.Y) * multiplier;
        targetPitch     = Mathf.Clamp(targetPitch, minFreePitch, maxFreePitch);

        currentRotation = targetRotation;
        currentPitch    = targetPitch;

        CursorController.ReturnCursorToGivenPoint(mouseHiddenOrigin);

        MaintainRotationBounds();
    }

    public  void StartMouseRotation() {
        rotating = true;
        if (OnStartedRotating != null) OnStartedRotating.Invoke();
        
        CursorController.HideCursor(HideCursorRequesterType.MouseRotation);
        mouseHiddenOrigin = Cursor.Position;
    }

    public  void StopMouseRotation() {
        if (!rotating) return;

        rotating = false;
        if (OnStoppedRotating != null) OnStoppedRotating.Invoke();

        CursorController.ShowCursor(HideCursorRequesterType.MouseRotation);
        CursorController.ReturnCursorToLastHidden();
    }

    /// <summary>
    /// Performs the damping calculations for all common variables - rotation, pitch, distance, offset.
    /// </summary>
    private void PerformAllDampings() {
        PerformDistanceDamping();
        currentRotation = Mathf.SmoothDamp   (currentRotation, targetRotation, ref currentRotationVel, dampTime, 50000, Time.fixedDeltaTime);
        currentPitch    = Mathf.SmoothDamp   (currentPitch,    targetPitch,    ref currentPitchVel,    dampTime, 50000, Time.fixedDeltaTime);
        currentOffset   = Vector3.SmoothDamp (currentOffset,   targetOffset,   ref currentOffsetVel,   dampTime, 50000, Time.fixedDeltaTime);
    }

    private void PerformDistanceDamping() {
        currentDistance = Mathf.SmoothDamp (currentDistance, targetDistance, ref currentDistanceVel, dampTime, 50000, Time.fixedDeltaTime);
    }

    /// <summary>
    /// Performs the calculation to force the camera at the correct position and rotation based on 'current' values.
    /// </summary>
    private void PerformCameraPositioning() {
        // Set camera distance
        Vector3 pos = new Vector3(0, 0, -currentDistance);

        // Rotate with 'current' values
        pos = Quaternion.Euler(currentPitch, currentRotation, 0) * pos;

        // Set final position ignoring offset
        pos += targetPoint;
        transform.position = pos;

        // Set final rotation
        transform.LookAt(targetPoint);

        // Fix offset's values based on rotation
        Vector3 rotatedOffset = Quaternion.AngleAxis(currentRotation, Vector3.up) * currentOffset;

        // Add to final position
        transform.position += rotatedOffset;
    }

    /// <summary>
    /// Linecast collision fixing between target and camera.
    /// </summary>
    private void CameraCollisionFixer(Vector3 target) {
        RaycastHit hit;

        if (Physics.Linecast(target, transform.position, out hit, cameraCollisiongMask)) {
            if (!colliding) {
                colliding = true;
                collisionFixDistanceVel = 0;
            }

            Vector3 v = target - transform.position;
            v = hit.point + v.normalized * collisionOffset;
            collisionFixDistance = Vector3.Distance(v, transform.position);
            transform.position = v;
        }
        else {
            if (colliding) {
                colliding = false;
            }

            Vector3 relativeForward = (target - transform.position).normalized;
            collisionFixDistance = Mathf.SmoothDamp(collisionFixDistance, 0, ref collisionFixDistanceVel, 0.1f, 5000, Time.unscaledDeltaTime);
            transform.position = transform.position + relativeForward * collisionFixDistance;
        }
    }

    /// <summary>
    /// Overhead movement to avoid visual collision with the target model.
    /// </summary>
    private void TryPerformOverhead(float heightOffset) {
        float totalHeight = target.position.y + heightOffset + overheadHeight;
        if (transform.position.y > totalHeight) return;

        // Store the position of the target at camera height
        Vector3 targetAtCamHeight = target.position;
        targetAtCamHeight.y = transform.position.y;

        // Get direction between target and camera on XZ
        Vector3 overheadDir = targetAtCamHeight - transform.position;

        // Store distance
        float sqrCurrentDist = overheadDir.sqrMagnitude;
        // If distance is too big, we can ignore overhead
        if (sqrCurrentDist > sqrOverheadDistance) return;

        // Calculate the overhead start point
        Vector3 overheadStart = targetAtCamHeight - overheadDir.normalized * overheadDistance;

        // and overhead end point
        Vector3 overheadEnd = target.transform.position;
        overheadEnd.y = totalHeight;

        // Calculate the current t value inside overhead distance
        float t = sqrCurrentDist / sqrOverheadDistance;

        // Set the actual position height between overhead start and overhead end
        // using the t value with a quadratic bezier curve
        Vector3 pos = transform.position;
        pos.y = Utils.Math.QBezier(overheadEnd.y, overheadStart.y, overheadEnd.y, t);

        transform.position = pos;
    }
    
    private void MaintainRotationBounds() {
        if (targetRotation > 360) {
            targetRotation  -= 360;
            currentRotation -= 360;
        }
        else if (targetRotation < 0) {
            targetRotation  += 360;
            currentRotation += 360;
        }
    }
}