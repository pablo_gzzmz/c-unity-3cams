
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Simple placeholder for the player.
/// </summary>

public class LocalPlayer : Singleton<LocalPlayer> {
    
    [SerializeField]
    private PlayerViewManager view;
    public  static PlayerViewManager View { get { return instance.view; } }

    [SerializeField]
    private MoverDirectional mover;
    public  static MoverDirectional Mover { get { return instance.mover; } }
    
}
