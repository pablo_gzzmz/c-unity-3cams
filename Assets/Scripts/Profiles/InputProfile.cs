
using UnityEngine;
using System;

[CreateAssetMenu(fileName = "Input Profile", menuName = "Profiles/Input Profile")]
public class InputProfile : ScriptableObject{

    [Header("Keyboard")]
    [SerializeField]
    private KeyCode moveFront = KeyCode.W;
    public  KeyCode MoveFront { get { return moveFront; } }
    [SerializeField]
    private KeyCode moveBack  = KeyCode.S;
    public  KeyCode MoveBack { get { return moveBack; } }
    [SerializeField]
    private KeyCode moveLeft  = KeyCode.A;
    public  KeyCode MoveLeft { get { return moveLeft; } }
    [SerializeField]
    private KeyCode moveRight = KeyCode.D;
    public  KeyCode MoveRight { get { return moveRight; } }

    [Space(6)]
    [SerializeField]
    private FullKeyCode sprint;
    public  FullKeyCode Sprint { get { return sprint; } }

    [Space(10)]
    [SerializeField]
    private FullKeyCode modeThirdPerson;
    public  FullKeyCode ModeThirdPerson { get { return modeThirdPerson; } }
    [SerializeField]
    private FullKeyCode modeTopDown;
    public  FullKeyCode ModeTopDown { get { return modeTopDown; } }
    [SerializeField]
    private FullKeyCode modeFree;
    public  FullKeyCode ModeFree { get { return modeFree; } }

    [Space(10)]
    [SerializeField]
    private FullKeyCode offsetLeft;
    public  FullKeyCode OffsetLeft { get { return offsetLeft; } }

    [SerializeField]
    private FullKeyCode offsetRight;
    public  FullKeyCode OffsetRight { get { return offsetRight; } }
}

[Serializable]
public struct FullKeyCode {
    [SerializeField]
    private KeyCode mainKey;
    [SerializeField]
    private KeyCode gamepadKey;

    public KeyCode Get { get { return Config.Gamepad ? gamepadKey : mainKey; } }
}