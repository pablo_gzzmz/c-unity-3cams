
using UnityEngine;

/// <summary>
/// Serialized config options.
/// </summary>

[CreateAssetMenu(fileName = "Config Profile", menuName = "Profiles/Config Profile")]
public class ConfigProfile : ScriptableObject {

    [Range(10, 100)]
    [SerializeField]
    private int   freeCameraSpeed = 50;
    private float m_freeCameraSpeed;
    public  float FreeCameraSpeed { get { return m_freeCameraSpeed; } }

    [Range(1, 10)]
    [SerializeField]
    private int   mouseSensitivity = 5;
    private float m_mouseSensitivity;
    public  float MouseSensitivity { get { return m_mouseSensitivity; } }

    [SerializeField]
    private bool lockMouseMovement = false;
    public  bool LockMouseMovement { get { return lockMouseMovement; } }

    [SerializeField]
    private bool gamepad = false;
    public  bool Gamepad { get { return gamepad; } }

    [Range(1, 10)]
    [SerializeField]
    private int  gamepadSensitivity = 5;
    private float m_gamepadSensitivity;
    public  float GamepadSensitivity { get { return m_gamepadSensitivity; } }

    public void Set() {
        m_freeCameraSpeed    = freeCameraSpeed    * 0.30f;
        m_mouseSensitivity   = mouseSensitivity   * 0.05f;
        m_gamepadSensitivity = gamepadSensitivity * 0.025f;
    }

    private void OnValidate() {
        Set();
    }
}
