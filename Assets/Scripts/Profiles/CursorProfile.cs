
using UnityEngine;

/// <summary>
/// Serialized textures for all cursors.
/// </summary>

[CreateAssetMenu(fileName = "Cursor Profile", menuName = "Profiles/Cursor Profile")]
public class CursorProfile : ScriptableObject {

	[SerializeField]
    private Texture2D defaultCursor = null;
    public  Texture2D Default { get { return defaultCursor; } }
}
