
using System;
using System.Collections.Generic;
using System.Drawing;
using UnityEngine;

// The type of request sources for cursor hiding
public enum HideCursorRequesterType {
    MouseRotation,
    LockMode
}

// Every possible cursor variation
public enum CursorType {
    Default
}

public class CursorController : MonoBehaviour {

    private static CursorController instance;

    private static CursorProfile profile;

    private static List<HideCursorRequesterType> mouseHiddenRequesters = new List<HideCursorRequesterType>();
    public  static bool CursorHidden { get { return mouseHiddenRequesters.Count > 0; } }

    /// <summary> Point used to store last visible mouse position when cursor is hidden. </summary>
    private static Point hiddenMousePoint;

    public static event Action OnCursorHidden;
    public static event Action OnCursorVisible;

    private void Awake() {
        if (instance != null) DestroyImmediate(this);
        instance = this;

        profile = Resources.Load<CursorProfile>("Profiles/Cursor Profile");

        SetCursorSprite(CursorType.Default);
    }

    public static void SetCursorSprite(CursorType type) {
        switch (type) {
            case CursorType.Default:
                Cursor.SetCursor(profile.Default, Vector2.zero, CursorMode.Auto);
                break;
        }
    }
    
    public static void HideCursor(HideCursorRequesterType type) {
        // Always set hidden mouse points as the last request
        hiddenMousePoint = System.Windows.Forms.Cursor.Position;
        
        if (mouseHiddenRequesters.Contains(type)) {
            return;
        }
        else {
            mouseHiddenRequesters.Add(type);
        }

        Cursor.visible = false;
        if (OnCursorHidden != null) OnCursorHidden.Invoke();
    }

    public static void ShowCursor(HideCursorRequesterType type) {
        if (mouseHiddenRequesters.Contains(type)) {
            mouseHiddenRequesters.Remove(type);
        }
        else {
            return;
        }
        if (CursorHidden) return;

        Cursor.visible = true;
        if (OnCursorVisible != null) OnCursorVisible.Invoke();
    }

    public static void ReturnCursorToLastHidden() {
        System.Windows.Forms.Cursor.Position = hiddenMousePoint;
    }
    public static void ReturnCursorToGivenPoint(Point point) {
        System.Windows.Forms.Cursor.Position = point;
    }
}
