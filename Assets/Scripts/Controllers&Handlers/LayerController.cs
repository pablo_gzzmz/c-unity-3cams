
using UnityEngine;

/// <summary>
/// Sets and returns layer data.
/// </summary>
public static class LayerController{
    
    private static LayerMask groundMask;
    public  static LayerMask GroundMask { get { return groundMask; } }
    private static int groundValue;
    public  static int GroundValue { get { return groundValue; } }

    static LayerController() {
        groundValue = LayerMask.NameToLayer("Ground");
        groundMask  = 1 << groundValue;
    }
}
