
using UnityEngine;

/// <summary>
/// Singleton to test different input.
/// </summary>

public class InputController : Singleton<InputController> {
    
    private static InputProfile profile;
    
    private static PlayerInputMode inputMode;

    private static float zAxisInput;
    private static float xAxisInput;

    private const float dampSensitivityMultiplier = 10;

    protected override void SingletonAwake() {
        profile = Resources.Load<InputProfile>("Profiles/Input Profile");
    }

    private void Start() {
        SetInputMode();

        LocalPlayer.View.SetThirdPersonXOffset(Globals.thirdPersonSideOffsetAmount);
    }

    private void Update() {
        SetMovementInput();

        InputModeCheck();

        switch (inputMode) {
            case PlayerInputMode.ThirdPerson:
                ThirdPersonRotationInput ();
                ThirdPersonOffsetterInput();
                SprintInput();
                break;

            case PlayerInputMode.TopDown:
                SprintInput();
                break;

            case PlayerInputMode.Free:
                LocalPlayer.View.MoveFreeTarget(zAxisInput, xAxisInput);

                LocalPlayer.View.SetFreeZoom(Input.GetAxis("Mouse ScrollWheel"));

                FreeCameraMouseInput();
                break;
        }

        // Testing
        if (Input.GetKeyDown(KeyCode.Escape)) {
            bool b = !LocalPlayer.View.EnableInput;
            LocalPlayer.View.EnableInput = b;
        }
    }

    private void FixedUpdate() {
        switch (inputMode) {
            case PlayerInputMode.ThirdPerson:
            case PlayerInputMode.TopDown:
                LocalPlayer.Mover.Move(zAxisInput, xAxisInput);
                break;
        }
    }

    private void InputModeCheck() {
        if (Input.GetKeyDown(profile.ModeThirdPerson.Get)) {
            inputMode = PlayerInputMode.ThirdPerson;
            SetInputMode();
        }
        else
        if (Input.GetKeyDown(profile.ModeTopDown.Get)) {
            inputMode = PlayerInputMode.TopDown;
            SetInputMode();
        }
        else
        if (Input.GetKeyDown(profile.ModeFree.Get)) {
            inputMode = PlayerInputMode.Free;
            SetInputMode();
        }
    }

    private void SetMovementInput() {
        if (Config.Gamepad) {
            zAxisInput = Input.GetAxis("Gamepad LY");
            xAxisInput = Input.GetAxis("Gamepad LX");
        }
        else {
            zAxisInput = Input.GetKey(profile.MoveFront) ? 1 : Input.GetKey(profile.MoveBack) ? -1 : 0;
            xAxisInput = Input.GetKey(profile.MoveRight) ? 1 : Input.GetKey(profile.MoveLeft) ? -1 : 0;
        }
    }

    private void ThirdPersonRotationInput() {
        PlayerViewManager view = LocalPlayer.View;

        if (!view.EnableInput) return;

        float sensitivity;
        float lookRotation;
        float lookPitch;

        if (Config.Gamepad) {
            sensitivity = Config.GamepadSensitiviy;
            lookRotation = Input.GetAxis("Gamepad RX");
            lookPitch    = Input.GetAxis("Gamepad RY");
        }
        else {
            sensitivity = Config.MouseSensitivity;
            lookRotation = Input.GetAxis("Mouse X");
            lookPitch    = Input.GetAxis("Mouse Y");
        }
        
        lookRotation *= sensitivity * dampSensitivityMultiplier;
        lookPitch *= view.InvertY ? -1 : 1;
        lookPitch *= sensitivity * dampSensitivityMultiplier;

        view.AddThirdPersonRotation(lookRotation, lookPitch);
    }

    private void ThirdPersonOffsetterInput() {
        if (Input.GetKeyDown(profile.OffsetLeft.Get)) {
            LocalPlayer.View.SetThirdPersonXOffset(-Globals.thirdPersonSideOffsetAmount);
        }
        else 
        if (Input.GetKeyDown(profile.OffsetRight.Get)) {
            LocalPlayer.View.SetThirdPersonXOffset(Globals.thirdPersonSideOffsetAmount);
        }
    }

    private void FreeCameraMouseInput() {
        // Right click
        if (Input.GetMouseButtonDown(1)) {
            LocalPlayer.View.StartMouseRotation();
        }
        else
        if (Input.GetMouseButtonUp(1)) {
            LocalPlayer.View.StopMouseRotation();
        }
    }

    private void SetInputMode() {
        switch (inputMode) {
            case PlayerInputMode.ThirdPerson:
                LocalPlayer.View.SetCameraMode(CameraMode.ThirdPerson);
                CursorController.HideCursor(HideCursorRequesterType.LockMode);
                break;

            case PlayerInputMode.TopDown:
                LocalPlayer.View.SetCameraMode(CameraMode.TopDown);
                CursorController.HideCursor(HideCursorRequesterType.LockMode);
                break;

            case PlayerInputMode.Free:
                LocalPlayer.View.SetCameraMode(CameraMode.Free);
                CursorController.ShowCursor(HideCursorRequesterType.LockMode);
                break;
        }
    }

    private void SprintInput() {
        if (Input.GetKeyDown(profile.Sprint.Get)) {
            LocalPlayer.Mover.Sprinting = true;
        }
        else
        if (Input.GetKeyUp(profile.Sprint.Get)) {
            LocalPlayer.Mover.Sprinting = false;
        }
    }
}
