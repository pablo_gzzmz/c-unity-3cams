
using UnityEngine;
using System;

/// <summary>
/// Used as a static class for basic Screen functionality.
/// </summary>

public class ScreenController : MonoBehaviour {

    private static ScreenController instance;

    private static int halfScreenWidth  = 0;
    public  static int HalfScreenWidth { get { return halfScreenWidth; } }
    private static int halfScreenHeight = 0;
    public  static int HalfScreenHeight { get { return halfScreenHeight; } }

    public static event Action OnScreenSizeChange;

    private static float previousScreenWidth = 0;
    private static float previousScreenHeight = 0;

    private void Awake() {
        if (instance != null) Destroy(this);

        instance = this;
    }

    private void Update() {
        if (Screen.width != previousScreenWidth || Screen.height != previousScreenHeight) {
            halfScreenWidth  = Screen.width / 2;
            halfScreenHeight = Screen.height / 2;

            if (OnScreenSizeChange != null) OnScreenSizeChange.Invoke();
        }
        previousScreenWidth = Screen.width; previousScreenHeight = Screen.height;
    }
}
